package de.vdw.io.enocean.eep.eep26.profiles.A5.A508;

import de.vdw.io.enocean.eep.eep26.attributes.EEP26Brightness;
import java.util.ArrayList;
import uk.co._4ng.enocean.devices.DeviceManager;
import uk.co._4ng.enocean.devices.EnOceanDevice;
import uk.co._4ng.enocean.eep.EEPAttribute;
import uk.co._4ng.enocean.eep.EEPAttributeChangeJob;
import uk.co._4ng.enocean.eep.eep26.attributes.EEP26PIRStatus;
import uk.co._4ng.enocean.eep.eep26.attributes.EEP26SupplyVoltage;
import uk.co._4ng.enocean.eep.eep26.telegram.EEP26Telegram;
import uk.co._4ng.enocean.eep.eep26.telegram.EEP26TelegramType;
import uk.co._4ng.enocean.eep.eep26.telegram.FourBSTelegram;

public class A50801 extends A508 {
  public static final int CHANNEL = 0;

  public A50801() {
    addChannelAttribute(CHANNEL, new EEP26SupplyVoltage(0.0, 5.0));
    addChannelAttribute(CHANNEL, new EEP26Brightness());
    addChannelAttribute(CHANNEL, new EEP26PIRStatus());
  }

  @Override
  protected boolean handleProfileUpdate(
      DeviceManager deviceManager, EEP26Telegram telegram, EnOceanDevice device) {
    boolean success = false;

    if (telegram.getTelegramType() == EEP26TelegramType.FourBS) {

      // cast the telegram to handle to its real type
      FourBSTelegram profileUpdate = (FourBSTelegram) telegram;

      // get the packet payload
      byte[] payload = profileUpdate.getPayload();

      A5BrightnessOccupancySensingMessage message =
          new A5BrightnessOccupancySensingMessage(payload);

      if (message.isValid()) {
        // prepare the list of changed attributes (only one)
        ArrayList<EEPAttribute<?>> changedAttributes = new ArrayList<>();

        // supply voltage
        EEP26SupplyVoltage supplyVoltage =
            (EEP26SupplyVoltage) getChannelAttribute(CHANNEL, EEP26SupplyVoltage.NAME);

        // if the supply voltage attribute exists and a valid value
        // had been specified in the message
        if (supplyVoltage != null) {
          // store the voltage value
          supplyVoltage.setRawValue(message.getSupplyVoltage());

          // update the list of changed attributes
          changedAttributes.add(supplyVoltage);
        }

        // occupancy status
        EEP26PIRStatus pirStatus =
            (EEP26PIRStatus) getChannelAttribute(CHANNEL, EEP26PIRStatus.NAME);

        EEP26Brightness brightness =
            (EEP26Brightness) getChannelAttribute(CHANNEL, EEP26Brightness.NAME);

        if (brightness != null) {
          brightness.setRawValue(message.getBrightness());

          // update the list of changed attributes
          changedAttributes.add(brightness);
        }

        // set the pir status if the corresponding attribute is
        // available
        if (pirStatus != null) {
          // set the pir status value
          pirStatus.setValue(message.isMotionDetected());

          // update the list of changed attributes
          changedAttributes.add(pirStatus);
        }

        // if some attribute changed, notify it to listeners
        if (!changedAttributes.isEmpty()) {
          // build the dispatching task
          EEPAttributeChangeJob dispatcherTask =
              new EEPAttributeChangeJob(
                  deviceManager, changedAttributes, CHANNEL, telegram, device);

          // submit the task for execution
          attributeNotificationWorker.submit(dispatcherTask);

          // set success true
          success = true;
        }
      }
    }

    return success;
  }
}
