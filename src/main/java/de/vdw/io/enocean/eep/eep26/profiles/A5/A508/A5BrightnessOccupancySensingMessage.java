package de.vdw.io.enocean.eep.eep26.profiles.A5.A508;

import lombok.Data;

@Data
class A5BrightnessOccupancySensingMessage {
  // the supply voltage level, if available
  private int supplyVoltage;

  private int brightness;

  // the motion detection flag
  private boolean motionDetected;

  // the validity flag
  private boolean valid;

  /**
   * Class constructor, given the telegram payload parses the content and sets up the internal
   * fields to reflect the payload content in a more accessible form.
   *
   * @param data the 4BS telegram payload as an array of byte.
   */
  A5BrightnessOccupancySensingMessage(byte[] data) {
    // initially not valid
    this.valid = false;

    // check the data length, shall be 4
    if (data.length == 4) {
      // decode the supply voltage availability flag
      this.brightness = data[1] & 0x00ff;

      // if supply voltage is available, get the voltage value (first data
      // byte)
      byte supplyVoltageAsByte = data[0];

      // convert the value to an integer
      this.supplyVoltage = supplyVoltageAsByte & 0x00ff;

      // decode the pir status
      byte pirStatusAsByte = data[3];

      // convert to a boolean value
      this.motionDetected = pirStatusAsByte == 16;

      // everything fine....
      this.valid = true;
    }
  }
}
