package de.vdw.io.enocean.eep.eep26.attributes;

import java.nio.ByteBuffer;
import lombok.Getter;
import lombok.Setter;
import uk.co._4ng.enocean.eep.EEPAttribute;

public class EEP26Brightness extends EEPAttribute<Double> {
  // the EEPFunction name
  public static final String NAME = "Brightness";

  // the allowed range
  @Getter @Setter private double minBrightness;
  @Getter @Setter private double maxBrightness;

  public EEP26Brightness() {
    super(NAME);

    // set the default value
    this.value = 0.0;
    this.unit = "lux";
    this.minBrightness = 0;
    this.maxBrightness = 2048;
  }

  public EEP26Brightness(Double minBrightness, Double maxBrightness) {
    super(NAME);

    // default value 0V
    this.value = 0.0;
    this.unit = "V";
    this.minBrightness = minBrightness;
    this.maxBrightness = maxBrightness;
  }

  @Override
  public byte[] byteValue() {

    ByteBuffer valueAsBytes = ByteBuffer.wrap(new byte[4]);

    // store the current value
    valueAsBytes.putDouble(this.value);

    // return the value as byte array
    return valueAsBytes.array();
  }

  @Override
  public void setRawValue(int value) {
    if (value >= 0 && value <= 250) {
      this.value = (this.maxBrightness - this.minBrightness) * value / 250.0 + this.minBrightness;
    }
  }

  /**
   * Checks if the current attribute represents a value in the declared valid range or not.
   *
   * @return True if the value is valid
   */
  public boolean isValid() {
    return this.value >= this.minBrightness && this.value <= this.maxBrightness;
  }
}
