package de.vdw.io.enocean2mqtt.enocean.listener;

import javax.inject.Singleton;
import de.vdw.io.enocean2mqtt.services.HomeAssistantDeviceService;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import uk.co._4ng.enocean.devices.EnOceanDevice;

@Slf4j
@Singleton
@Value
public class DeviceListener implements uk.co._4ng.enocean.communication.DeviceListener {
  /**
   * Identifying device as well as creating new ones.
   */
  HomeAssistantDeviceService homeAssistantDeviceService;

  @Override
  public void addedEnOceanDevice(EnOceanDevice device) {
    log.debug("Added device: {} ({})", device.getAddressHex(), device.getEEP().getIdentifier());

    homeAssistantDeviceService.addNewDevice(device);
  }

  @Override
  public void modifiedEnOceanDevice(EnOceanDevice changedDevice) {
    log.info("Modified device: {} ({})", changedDevice.getAddressHex(),
        changedDevice.getEEP().getIdentifier());
  }

  @Override
  public void removedEnOceanDevice(EnOceanDevice changedDevice) {
    log.info("Removed device: {} ({})", changedDevice.getAddressHex(),
        changedDevice.getEEP().getIdentifier());
  }

}
