package de.vdw.io.enocean2mqtt.enocean.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.inject.Singleton;
import de.vdw.io.enocean2mqtt.utils.HexUtils;
import lombok.extern.slf4j.Slf4j;
import uk.co._4ng.enocean.protocol.serial.v3.network.packet.ESP3Packet;
import uk.co._4ng.enocean.util.EnOceanUtils;

@Slf4j
@Singleton
public class PacketListener implements uk.co._4ng.enocean.link.PacketListener {

  @Override
  public void handlePacket(ESP3Packet pkt) {
    log.debug("Packet received: {}", EnOceanUtils.toHexString(pkt.getDataPayload()));

    List<String> intValues = new ArrayList<>(pkt.getData().length);
    for (int index = 0; index < pkt.getData().length; index++) {
      byte b = pkt.getData()[index];
      intValues.add(index, String.valueOf(Byte.toUnsignedInt(b)));
    }
    int dataLength = (int) HexUtils.bytesToLong(pkt.getDataLength());
    log.trace("Data length: {}, data: {}", dataLength, EnOceanUtils.toHexString(pkt.getData()));

    byte[] address = Arrays.copyOfRange(pkt.getData(), dataLength - 5, dataLength - 1);
    byte[] rorg = Arrays.copyOfRange(pkt.getData(), 0, 1);

    log.trace("Address: {}, RORG: {}", EnOceanUtils.toHexString(address),
        EnOceanUtils.toHexString(rorg));

    log.trace("Data values: {}", String.join(" ", intValues));

    log.trace("Optional length: {}, optional: {}", Byte.toUnsignedLong(pkt.getOptLength()),
        EnOceanUtils.toHexString(pkt.getOptData()));

    log.trace("Complete package: {}", EnOceanUtils.toHexString(pkt.getPacketAsBytes()));
  }
}
