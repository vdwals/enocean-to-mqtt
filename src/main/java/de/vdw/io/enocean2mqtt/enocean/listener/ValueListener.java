package de.vdw.io.enocean2mqtt.enocean.listener;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Singleton;
import de.vdw.io.enocean2mqtt.services.HomeAssistantDeviceService;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import uk.co._4ng.enocean.communication.DeviceValueListener;
import uk.co._4ng.enocean.devices.EnOceanDevice;
import uk.co._4ng.enocean.eep.EEPAttributeChangeJob;
import uk.co._4ng.enocean.eep.EEPAttributeChangeJob.EEPAttributeChange;
import uk.co._4ng.enocean.util.EnOceanUtils;

@Slf4j
@Singleton
@Value
public class ValueListener implements DeviceValueListener {
  /**
   * Update value of device.
   */
  HomeAssistantDeviceService homeAssistantDeviceService;

  @Override
  public void deviceAttributeChange(EEPAttributeChangeJob eepAttributeChangeJob) {
    for (EEPAttributeChange eepAttributeChange : eepAttributeChangeJob.getChanges()) {

      log.debug("Device update received: {}", eepAttributeChange.getDevice().getAddressHex());
      log.debug("Attribute changed: {} = {} {}", eepAttributeChange.getAttribute().getName(),
          eepAttributeChange.getAttribute().getValue(),
          eepAttributeChange.getAttribute().getUnit());
      log.debug("Payload: {}",
          EnOceanUtils.toHexString(eepAttributeChange.getTelegram().getPayload()));
      log.debug("Channel: {}", eepAttributeChange.getChannelId());
    }

    Map<EnOceanDevice, List<EEPAttributeChange>> changeMap = eepAttributeChangeJob.getChanges()
        .stream().collect(Collectors.groupingBy(EEPAttributeChange::getDevice));

    homeAssistantDeviceService.notifyValueUpdates(changeMap);
  }

}
