package de.vdw.io.enocean2mqtt.enocean.listener;

import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import uk.co._4ng.enocean.devices.EnOceanDevice;

@Singleton
@Slf4j
public class TeachInListener implements uk.co._4ng.enocean.communication.TeachInListener {

  @Override
  public void foundNewDevice(EnOceanDevice device) {
    log.info("New device found: {}", device.toString());
  }

  @Override
  public void foundRegisteredDevice(EnOceanDevice device) {
    log.info("New device registered: {}", device.toString());
  }

  @Override
  public void teachInDisabled() {
    log.info("Teach-In disabled");
  }

  @Override
  public void teachInEnabled() {
    log.info("Teach-In enabled");
  }

}
