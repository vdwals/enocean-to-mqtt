package de.vdw.io.enocean2mqtt.profiles;

import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.Device.DeviceBuilder;
import uk.co._4ng.enocean.devices.EnOceanDevice;

public abstract class AbstractDeviceMapper implements DeviceMapper {

  public Device buildDevice(EnOceanDevice enoceanDevice) {
    String model = enoceanDevice.getEEP().getIdentifier().toString();
    String name = "enocean_" + enoceanDevice.getAddressHex();

    DeviceBuilder deviceBuilder = Device.builder().model(model).name(name)
        .nodeId(enoceanDevice.getAddressHex()).identifier(enoceanDevice.getAddressHex());

    if (enoceanDevice.getManufacturerId() != null) {
      deviceBuilder.manufacturer(String.valueOf(enoceanDevice.getManufacturerUID()));
    }

    return deviceBuilder.build();
  }

}
