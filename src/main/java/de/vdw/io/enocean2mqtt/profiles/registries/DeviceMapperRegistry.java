package de.vdw.io.enocean2mqtt.profiles.registries;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang3.StringUtils;
import de.vdw.io.enocean2mqtt.profiles.DeviceMapper;
import de.vdw.io.enocean2mqtt.profiles.F6.F602.F60201;
import lombok.extern.slf4j.Slf4j;
import uk.co._4ng.enocean.eep.EEP;

@Slf4j
public class DeviceMapperRegistry {
  public static final Map<String, DeviceMapper> supportedMappers = new ConcurrentHashMap<>();

  static {
    addProfile(F60201.class);
  }

  private static void addProfile(Class<? extends DeviceMapper> profile) {
    if (profile != null) {
      try {
        DeviceMapper mapperSerivce = (DeviceMapper) profile.getConstructors()[0].newInstance();
        supportedMappers.put(profile.getSimpleName(), mapperSerivce);
      } catch (Exception e) {
        log.error("Cannot instantiate EEP profile {}", profile, e);
      }
    }
  }

  public static DeviceMapper getDeviceMapper(EEP profile) {
    String identifier = StringUtils.remove(profile.getIdentifier().toString(), "-");
    DeviceMapper deviceMapper = supportedMappers.get(identifier);

    if (deviceMapper == null) {
      log.error("Cannot map device with profile {} with identifier {}", profile, identifier);
    }

    return deviceMapper;
  }

}
