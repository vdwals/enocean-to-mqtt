package de.vdw.io.enocean2mqtt.profiles;

import de.vdw.it.hamqtt.devices.Device;
import uk.co._4ng.enocean.devices.EnOceanDevice;

public interface DeviceMapper {

  Device getDevice(EnOceanDevice enoceanDevice);
}
