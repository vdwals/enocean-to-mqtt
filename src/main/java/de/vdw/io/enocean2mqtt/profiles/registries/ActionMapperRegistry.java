package de.vdw.io.enocean2mqtt.profiles.registries;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import de.vdw.io.enocean2mqtt.profiles.ActionMapper;
import de.vdw.io.enocean2mqtt.profiles.F6.F602.F60201;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ActionMapperRegistry {
  public static final Map<String, ActionMapper> supportedMappers = new ConcurrentHashMap<>();

  static {

    addProfile(F60201.class);
  }

  private static void addProfile(Class<? extends ActionMapper> profile) {
    if (profile != null) {
      try {
        ActionMapper mapperSerivce = (ActionMapper) profile.getConstructors()[0].newInstance();
        supportedMappers.put(profile.getSimpleName(), mapperSerivce);
      } catch (Exception e) {
        log.error("Cannot instantiate EEP profile {}", profile, e);
      }
    }
  }

  public static ActionMapper getActionMapperService(String profile) {
    return supportedMappers.get(profile);
  }

}
