package de.vdw.io.enocean2mqtt.profiles;

import java.util.List;
import de.vdw.io.enocean2mqtt.services.MqttService;
import de.vdw.it.hamqtt.devices.Device;
import uk.co._4ng.enocean.eep.EEPAttributeChangeJob.EEPAttributeChange;

public interface ActionMapper {

  void mapActions(Device haDevice, List<EEPAttributeChange> attributeChanges,
      MqttService mqttService);
}
