package de.vdw.io.enocean2mqtt.profiles.registries;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import de.vdw.io.enocean2mqtt.profiles.AttributeMapper;
import de.vdw.io.enocean2mqtt.profiles.F6.F602.F60201;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AttributeMapperRegistry {
  public static final Map<String, AttributeMapper> supportedMappers = new ConcurrentHashMap<>();

  static {

    addProfile(F60201.class);
  }

  private static void addProfile(Class<? extends AttributeMapper> profile) {
    if (profile != null) {
      try {
        AttributeMapper mapperSerivce =
            (AttributeMapper) profile.getConstructors()[0].newInstance();
        supportedMappers.put(profile.getSimpleName(), mapperSerivce);
      } catch (Exception e) {
        log.error("Cannot instantiate EEP profile {}", profile, e);
      }
    }
  }

  public static AttributeMapper getAttributeMapperService(String profile) {
    return supportedMappers.get(profile);
  }

}
