package de.vdw.io.enocean2mqtt.profiles.F6.F602;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import de.vdw.io.enocean2mqtt.profiles.AbstractDeviceMapper;
import de.vdw.io.enocean2mqtt.profiles.ActionMapper;
import de.vdw.io.enocean2mqtt.profiles.AttributeMapper;
import de.vdw.io.enocean2mqtt.services.MqttService;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.entities.DeviceTrigger;
import de.vdw.it.hamqtt.devices.entities.DeviceTriggerAction;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import uk.co._4ng.enocean.devices.EnOceanDevice;
import uk.co._4ng.enocean.eep.EEPAttributeChangeJob.EEPAttributeChange;
import uk.co._4ng.enocean.eep.eep26.attributes.EEP26RockerSwitch2RockerAction;
import uk.co._4ng.enocean.eep.eep26.attributes.EEP26RockerSwitch2RockerEnergyBow;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class F60201 extends AbstractDeviceMapper implements AttributeMapper, ActionMapper {
  static final String PRESS = "press";
  static final String RELEASE = "release";
  static final String DELIMITER = "_";

  Map<String, DeviceTrigger> pressedTriggers = new HashMap<>();

  private String buildSubType(String type, int button) {
    return StringUtils.joinWith(DELIMITER, type, button);
  }

  private DeviceTrigger buildTrigger(String subType, String address) {
    return DeviceTrigger.builder().type(DeviceTriggerAction.ACTION).subtype(subType)
        .objectId(String.join(DELIMITER, subType)).build();
  }

  @Override
  public Device getDevice(EnOceanDevice enoceanDevice) {
    Device haDevice = buildDevice(enoceanDevice);

    DeviceTriggerAction dta = DeviceTriggerAction.builder()
        .objectId(String.join("_", haDevice.getName(), DeviceTriggerAction.ACTION)).device(haDevice)
        .build();
    haDevice.addEntity(dta);

    for (int i = 1; i <= 4; i++) {
      DeviceTrigger buttonPress =
          buildTrigger(buildSubType(PRESS, i), enoceanDevice.getAddressHex());
      dta.addDeviceTrigger(buttonPress);

      DeviceTrigger buttonRelease =
          buildTrigger(buildSubType(RELEASE, i), enoceanDevice.getAddressHex());
      dta.addDeviceTrigger(buttonRelease);
    }

    return haDevice;
  }

  @Override
  public void mapActions(Device haDevice, List<EEPAttributeChange> attributeChanges,
      MqttService mqttService) {

    /*
     * Check energy bow flag to determine if pressed or released.
     */
    boolean pressed = attributeChanges.stream()
        .filter(attributeChange -> attributeChange
            .getAttribute() instanceof EEP26RockerSwitch2RockerEnergyBow)
        .map(attributeChange -> (EEP26RockerSwitch2RockerEnergyBow) attributeChange.getAttribute())
        .anyMatch(EEP26RockerSwitch2RockerEnergyBow::getValue);

    String type = pressed ? PRESS : RELEASE;

    Optional<DeviceTrigger> deviceTrigger = Optional.empty();

    /*
     * If pressed, indentify device trigger and store as pressed.
     */
    if (pressed) {
      int button = -1;
      Optional<EEP26RockerSwitch2RockerAction> action =
          attributeChanges.stream().map(EEPAttributeChange::getAttribute)
              .filter(attribute -> attribute instanceof EEP26RockerSwitch2RockerAction)
              .map(attribute -> (EEP26RockerSwitch2RockerAction) attribute).findFirst();

      if (action.isPresent()) {
        for (int index = 0; index < action.get().getValue().length; index++) {
          if (action.get().getValue()[index]) {
            button = index + 1;
            break;
          }
        }
      }

      deviceTrigger = haDevice.getDeviceTriggerByTypeAndSubtype(DeviceTriggerAction.ACTION,
          buildSubType(type, button));

      /*
       * Store pressed trigger for release.
       */
      pressedTriggers.put(haDevice.getNodeId(), deviceTrigger.get());

    } else {
      /*
       * Get last pressed trigger for current button.
       */
      DeviceTrigger releasedTrigger = pressedTriggers.remove(haDevice.getNodeId());

      if (releasedTrigger != null)
        /*
         * Get lead release trigger for button.
         */
        deviceTrigger = haDevice.getDeviceTriggerByTypeAndSubtype(DeviceTriggerAction.ACTION,
            releasedTrigger.getSubtype().replace(PRESS, RELEASE));
    }

    /*
     * Trigger press or release.
     */
    if (deviceTrigger.isPresent()) {
      mqttService.publishTrigger(deviceTrigger.get());
    }
  }

  @Override
  public boolean mapAttributes(Device device, List<EEPAttributeChange> eepAttributeChanges) {
    return false;
  }

}
