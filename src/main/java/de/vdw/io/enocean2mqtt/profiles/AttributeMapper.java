package de.vdw.io.enocean2mqtt.profiles;

import java.util.List;
import de.vdw.it.hamqtt.devices.Device;
import uk.co._4ng.enocean.eep.EEPAttributeChangeJob.EEPAttributeChange;

public interface AttributeMapper {

  boolean mapAttributes(Device device, List<EEPAttributeChange> eepAttributeChanges);
}
