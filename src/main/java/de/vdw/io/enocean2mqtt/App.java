package de.vdw.io.enocean2mqtt;

import org.eclipse.paho.client.mqttv3.MqttException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.vdw.io.enocean.eep.eep26.profiles.A5.A508.A50801;
import de.vdw.io.enocean2mqtt.enocean.listener.DeviceListener;
import de.vdw.io.enocean2mqtt.enocean.listener.PacketListener;
import de.vdw.io.enocean2mqtt.enocean.listener.TeachInListener;
import de.vdw.io.enocean2mqtt.enocean.listener.ValueListener;
import de.vdw.io.enocean2mqtt.services.CommandService;
import de.vdw.io.enocean2mqtt.services.EnoceanDeviceService;
import de.vdw.io.enocean2mqtt.services.EnvironmentService;
import de.vdw.io.enocean2mqtt.services.HomeAssistantDeviceService;
import de.vdw.io.enocean2mqtt.services.MqttService;
import de.vdw.it.hamqtt.HomeAssistantMQTTService;
import eu.lestard.easydi.EasyDI;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import uk.co._4ng.enocean.communication.DeviceValueListener;
import uk.co._4ng.enocean.eep.eep26.EEPRegistry;

@Slf4j
@Value
public class App {

  public static void main(String[] args) throws MqttException {
    log.info("Starting Application");

    // Environment Service reads and manages system property access
    EnvironmentService environmentService = new EnvironmentService();

    // Ad all custom profiles
    EEPRegistry.addProfile(A50801.class);

    // Prepare interface bindings
    EasyDI easyDI = new EasyDI();
    easyDI.bindInterface(uk.co._4ng.enocean.communication.DeviceListener.class,
        DeviceListener.class);
    easyDI.bindInterface(DeviceValueListener.class, ValueListener.class);
    easyDI.bindInterface(uk.co._4ng.enocean.link.PacketListener.class, PacketListener.class);
    easyDI.bindInterface(uk.co._4ng.enocean.communication.TeachInListener.class,
        TeachInListener.class);

    log.info("Connect to MQTT-Broker");
    HomeAssistantMQTTService homeAssistantMQTTService =
        de.vdw.it.hamqtt.utils.ServiceFactory.createHomeAssistantMQTTService(
            environmentService.mqttHost(), environmentService.mqttPort(),
            environmentService.mqttUsername(), environmentService.mqttPassword(),
            environmentService.mqttTopic(), environmentService.mqttDiscoveryTopic(),
            "EnOcean Proxy", environmentService.mqttProtocoll());

    easyDI.bindInstance(HomeAssistantMQTTService.class, homeAssistantMQTTService);
    easyDI.bindInstance(EnvironmentService.class, environmentService);
    easyDI.bindInstance(ObjectMapper.class, new ObjectMapper());

    easyDI.markAsSingleton(MqttService.class);
    easyDI.markAsSingleton(HomeAssistantDeviceService.class);
    easyDI.markAsSingleton(EnoceanDeviceService.class);
    easyDI.markAsSingleton(CommandService.class);

    App app = easyDI.getInstance(App.class);

    app.init();
    app.start();
  }

  MqttService mqttService;

  CommandService commandService;

  EnoceanDeviceService enoceanDeviceService;

  public void init() {

    this.enoceanDeviceService.init();
  }

  public void start() {
    this.mqttService.start();
    this.enoceanDeviceService.start();
  }
}
