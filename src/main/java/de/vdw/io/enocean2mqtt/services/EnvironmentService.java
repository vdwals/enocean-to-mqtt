package de.vdw.io.enocean2mqtt.services;

import java.util.Map;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class EnvironmentService {
  static String DEFAULT_PROTOCOLL = "tcp";
  static String DEFAULT_DISCOVERY_TOPIC = "homeassistant";
  static String DEFAULT_TOPIC = "enocean_2_mqtt";
  static String MQTT_PROTOCOLL = "MQTT.PROTOCOLL";
  static String MQTT_DISCOVERY_TOPIC = "MQTT.DISCOVERY_TOPIC";
  static String MQTT_TOPIC = "MQTT.TOPIC";
  static String MQTT_PASSWORD = "MQTT.PASSWORD";
  static String MQTT_USERNAME = "MQTT.USERNAME";
  static String MQTT_HOST = "MQTT.HOST";
  static String MQTT_PORT = "MQTT.PORT";
  static String ENOCEAN_SERIAL_PORT = "ENOCEAN.SERIAL_PORT";

  Map<String, String> environmentVariables;

  public EnvironmentService() {
    environmentVariables = System.getenv();

    Configurator.setRootLevel(Level.INFO);

    if (environmentVariables.containsKey("LOG_LEVEL")) {
      switch (environmentVariables.get("LOG_LEVEL")) {
        case "TRACE":
          Configurator.setRootLevel(Level.TRACE);
          log.trace("Trace level logging activated");
          break;
        case "DEBUG":
          Configurator.setRootLevel(Level.DEBUG);
          log.debug("Debug level logging activated");
          break;
        default:
          log.warn("Unexpected value: " + environmentVariables.get("LOG_LEVEL"));
      }
    }

  }

  public String enoceanSerialPort() {
    return environmentVariables.get(ENOCEAN_SERIAL_PORT);
  }

  public String mqttDiscoveryTopic() {
    return environmentVariables.getOrDefault(MQTT_DISCOVERY_TOPIC, DEFAULT_DISCOVERY_TOPIC);
  }

  public String mqttHost() {
    return environmentVariables.get(MQTT_HOST);
  }

  public char[] mqttPassword() {
    String pw = environmentVariables.get(MQTT_PASSWORD);
    if (pw == null)
      return new char[0];
    return pw.toCharArray();
  }

  public String mqttPort() {
    return environmentVariables.get(MQTT_PORT);
  }

  public String mqttProtocoll() {
    return environmentVariables.getOrDefault(MQTT_PROTOCOLL, DEFAULT_PROTOCOLL);
  }

  public String mqttTopic() {
    return environmentVariables.getOrDefault(MQTT_TOPIC, DEFAULT_TOPIC);
  }

  public String mqttUsername() {
    return environmentVariables.get(MQTT_USERNAME);
  }
}
