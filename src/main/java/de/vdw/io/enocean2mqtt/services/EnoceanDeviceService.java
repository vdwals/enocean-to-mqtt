package de.vdw.io.enocean2mqtt.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.List;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.vdw.io.enocean2mqtt.dto.DeviceDto;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import uk.co._4ng.enocean.communication.Connection;
import uk.co._4ng.enocean.communication.DeviceListener;
import uk.co._4ng.enocean.communication.DeviceValueListener;
import uk.co._4ng.enocean.communication.TeachInListener;
import uk.co._4ng.enocean.devices.DeviceManager;
import uk.co._4ng.enocean.link.LinkLayer;
import uk.co._4ng.enocean.link.PacketListener;
import uk.co._4ng.enocean.util.EnOceanException;

@Value
@Slf4j
public class EnoceanDeviceService {
  private static final String DEVICE_FILE = "devices.json";

  DeviceManager deviceManager;
  Connection connection;
  LinkLayer linkLayer;
  ObjectMapper mapper;

  public EnoceanDeviceService(DeviceManager deviceManager, DeviceListener deviceListener,
      DeviceValueListener deviceValueListener, EnvironmentService environmentService,
      PacketListener packetListener, TeachInListener teachInListener, ObjectMapper mapper) {
    this.mapper = mapper;
    this.deviceManager = deviceManager;

    this.deviceManager.addDeviceListener(deviceListener);
    this.deviceManager.addDeviceValueListener(deviceValueListener);

    try {
      linkLayer = new LinkLayer(environmentService.enoceanSerialPort());
    } catch (EnOceanException e) {
      log.error(e.getMessage(), e);
      throw new InvalidParameterException("EnOcean Serieal Port is missing");
    }

    linkLayer.addPacketListener(packetListener);

    this.connection = new Connection(linkLayer, deviceManager);
    this.connection.getTeachIn().addTeachInListener(teachInListener);
  }

  public void init() {
    File deviceFile = new File(DEVICE_FILE);

    if (deviceFile.exists() && deviceFile.canRead()) {
      try {
        String devices = Files.readString(deviceFile.toPath());

        List<DeviceDto> deviceDtos = mapper.readValue(devices, new TypeReference<>() {});

        deviceDtos.forEach(
            device -> deviceManager.registerDevice(device.getAddress(), device.getProfile()));

      } catch (IOException e) {
        log.error(e.getMessage(), e);
      }
    }
  }

  public void start() {
    linkLayer.connect();
  }
}
