package de.vdw.io.enocean2mqtt.services;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Singleton;
import de.vdw.it.hamqtt.ICommandListener;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.entities.AbstractCommandEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CommandService implements ICommandListener {
  HomeAssistantDeviceService homeAssistantDeviceService;

  public CommandService(MqttService mqttService,
      HomeAssistantDeviceService homeAssistantDeviceService) {
    this.homeAssistantDeviceService = homeAssistantDeviceService;

    mqttService.addCommandListener(this);
  }

  @Override
  public List<Device> getDevices() {
    return homeAssistantDeviceService.getDeviceMap().values().stream()
        .filter(device -> device.getAvailableEntityStream()
            .anyMatch(entity -> entity instanceof AbstractCommandEntity))
        .collect(Collectors.toList());
  }

  @Override
  public void received(String topic, byte[] message) {
    log.debug("Command received on topic {}", topic);
    log.trace("Payload: {}", new String(message, StandardCharsets.UTF_8));
  }

}
