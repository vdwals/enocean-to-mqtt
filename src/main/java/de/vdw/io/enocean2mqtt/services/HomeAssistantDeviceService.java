package de.vdw.io.enocean2mqtt.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import de.vdw.io.enocean2mqtt.profiles.ActionMapper;
import de.vdw.io.enocean2mqtt.profiles.AttributeMapper;
import de.vdw.io.enocean2mqtt.profiles.DeviceMapper;
import de.vdw.io.enocean2mqtt.profiles.registries.ActionMapperRegistry;
import de.vdw.io.enocean2mqtt.profiles.registries.AttributeMapperRegistry;
import de.vdw.io.enocean2mqtt.profiles.registries.DeviceMapperRegistry;
import de.vdw.it.hamqtt.devices.Device;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import uk.co._4ng.enocean.devices.EnOceanDevice;
import uk.co._4ng.enocean.eep.EEPAttributeChangeJob.EEPAttributeChange;

@Value
@Slf4j
public class HomeAssistantDeviceService {
  MqttService mqttService;

  Map<String, Device> deviceMap = new HashMap<>();

  public void addNewDevice(EnOceanDevice device) {
    if (!deviceMap.containsKey(device.getAddressHex())) {
      DeviceMapper deviceMapper = DeviceMapperRegistry.getDeviceMapper(device.getEEP());

      if (deviceMapper == null) {
        log.error("Could not add new device {}", device);
        return;
      }

      Device haDevice = deviceMapper.getDevice(device);

      mqttService.addDevice(haDevice);

      deviceMap.put(device.getAddressHex(), haDevice);

      mqttService.publishDevices();
    }
  }

  public void notifyValueUpdates(Map<EnOceanDevice, List<EEPAttributeChange>> updateDeviceMap) {
    boolean anyUpdate = updateDeviceMap.entrySet().stream().anyMatch(entry -> {

      Device haDevice = deviceMap.get(entry.getKey().getAddressHex());
      List<EEPAttributeChange> attributeChanges = entry.getValue();

      String profile = StringUtils.remove(haDevice.getModel(), "-");

      ActionMapper actionMapper = ActionMapperRegistry.getActionMapperService(profile);
      if (actionMapper != null) {
        actionMapper.mapActions(haDevice, attributeChanges, mqttService);
      }

      AttributeMapper attributeMapper = AttributeMapperRegistry.getAttributeMapperService(profile);
      if (attributeMapper != null) {
        return attributeMapper.mapAttributes(haDevice, attributeChanges);
      }
      return false;
    });

    if (anyUpdate) {
      mqttService.publishValues();
    }
  }
}
