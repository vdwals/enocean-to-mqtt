package de.vdw.io.enocean2mqtt.services;

import javax.inject.Singleton;
import de.vdw.it.hamqtt.HomeAssistantMQTTService;
import de.vdw.it.hamqtt.ICommandListener;
import de.vdw.it.hamqtt.devices.Device;
import de.vdw.it.hamqtt.devices.entities.DeviceTrigger;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Singleton
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class MqttService {
  HomeAssistantMQTTService mqttService;

  public void addCommandListener(ICommandListener commandListener) {
    mqttService.addCommandListener(commandListener);
  }

  public void addDevice(Device device) {
    mqttService.addDevice(device);
  }

  public void publishDevices() {
    mqttService.publishConfigs();
  }

  public void publishTrigger(DeviceTrigger deviceTrigger) {
    mqttService.publishDeviceTrigger(deviceTrigger);
  }

  public void publishValues() {
    mqttService.publishValues();
  }

  public void start() {
    mqttService.connect();
  }

}
