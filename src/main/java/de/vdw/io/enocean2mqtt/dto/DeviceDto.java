package de.vdw.io.enocean2mqtt.dto;

import lombok.Value;

@Value
public class DeviceDto {
  String address, profile;
}
