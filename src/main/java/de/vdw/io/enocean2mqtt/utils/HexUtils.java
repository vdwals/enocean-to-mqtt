package de.vdw.io.enocean2mqtt.utils;

import java.math.BigInteger;
import lombok.experimental.UtilityClass;

@UtilityClass
public class HexUtils {

  public long bytesToLong(byte[] bytes) {
    return new BigInteger(bytes).longValue();
  }
}
